/**
 * @file
 * @brief Implementation of module WFAnalysis
 *
 * @copyright Copyright (c) 2020 CERN and the Corryvreckan authors.
 * This software is distributed under the terms of the MIT License, copied verbatim in the file "LICENSE.md".
 * In applying this license, CERN does not waive the privileges and immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 * SPDX-License-Identifier: MIT
 */

#include "WFAnalysis.h"
#include "objects/Pixel.hpp"
#include "objects/Waveform.hpp"

#include <Math/MinimizerOptions.h>
#include <TF1.h>
#include <TMath.h>
#include <TROOT.h>
#include <TSpline.h>

using namespace corryvreckan;

WFAnalysis::WFAnalysis(Configuration& config, std::shared_ptr<Detector> detector)
    : Module(config, detector), m_detector(detector) {}

void WFAnalysis::initialize() {

    config_.setDefault<double>("time_cut_frameedge", Units::get<double>(20, "ns"));
    config_.setDefault<double>("spatial_cut_sensoredge", 0.5);
    config_.setDefault<double>("chi2ndof_cut", 3.);
    config_.setDefault<double>("chargeCut", 3.);
    config_.setDefault<double>("chargeScale", 1000.);
    config_.setDefault<bool>("waveFit", false);
    config_.setDefault<bool>("use_closest_cluster", true);

    time_cut_frameedge_ = config_.get<double>("time_cut_frameedge");
    spatial_cut_sensoredge_ = config_.get<double>("spatial_cut_sensoredge");
    use_closest_cluster_ = config_.get<bool>("use_closest_cluster");
    chi2_ndof_cut_ = config_.get<double>("chi2ndof_cut");
    chargeCut_ = config_.get<double>("chargeCut");
    chargeScale_ = config_.get<double>("chargeScale");
    waveFit_ = config_.get<bool>("waveFit");

    // Some pixel properties
    auto pitch_x = m_detector->getPitch().X() * 1000.; // convert mm -> um
    auto pitch_y = m_detector->getPitch().Y() * 1000.; // convert mm -> um
    inpixelBinSize_ = ROOT::Math::XYPoint(0.5, 0.5);
    auto nbins_x = static_cast<int>(std::ceil(m_detector->getPitch().X() / inpixelBinSize_.x()));
    auto nbins_y = static_cast<int>(std::ceil(m_detector->getPitch().Y() / inpixelBinSize_.y()));

    for(auto& detector : get_detectors()) {
        LOG(DEBUG) << "Initialise for detector " + detector->getName();
    }

    directory = getROOTDirectory();
    local_directory_center = directory->mkdir("Center_Waveforms");
    local_directory_corner = directory->mkdir("Corner_Waveforms");

    // Histogram to draw all waveforms

    // All waveforms
    std::string title = m_detector->getName() + " Waveforms;time [ns];Signal [V]";
    Raw_waveform = new TH2F("Raw_waveform", title.c_str(), 2500, -300, -50, 2500, 0.05, 0.2);

    // Risetime histogram
    title = m_detector->getName() + " Risetime; time [ns];events";
    Rise_time = new TH1D("Risetime", title.c_str(), 10000, -1, 200);

      // Risetime histogram center
    title = m_detector->getName() + " Risetime_center; time [ns];events";
    Rise_time_center = new TH1D("Risetime", title.c_str(), 10000, -1, 200);

    // Risetime histogram corner
    title = m_detector->getName() + " Risetime_corner; time [ns];events";
    Rise_time_corner = new TH1D("Risetime", title.c_str(), 10000, -1, 200);

    // Amplitude histogram
    title = m_detector->getName() + " Amplitude; Voltage [V];events";
    Amplitude_hist = new TH1D("Amplitude", title.c_str(), 10000, -.001, 0.1);

    // Create a histogram to analyze a single waveform
    // title = m_detector->getName() + " Waveforms;time [ns];Signal [mV]";
    // Waveform_to_analyze = new TH1F("Waveform", "Waveform", 2500,-300,-50);

    title = "In pixel risetime;";
    In_pixel_risetime =
        new TProfile2D("In pixel risetime", title.c_str(), 81, -pitch_x / 2., pitch_x / 2., 81, -pitch_y / 2., pitch_y / 2.);

    // Map of pixels hit (that contain a waveform)
    title = "Pixels hit;";
    Pixel_Hits = new TH2D("Hit Map", title.c_str(), 2, -0.05, 1.05, 2, -0.05, 1.05);

    // Histogram of Amplitude vs risetime
    title = "Amplitude vs Risetime;";
    Amplitude_Risetime = new TH2D("Amplitude vs Risetime", title.c_str(), 1000, -0.005, 0.1, 1000, -1, 200);

    // Initialise member variables
    m_eventNumber = 0;
    count = 0;
}

// Geometric selection from DUT
bool WFAnalysis::acceptTrackDUT(const std::shared_ptr<Track>& track) {
    // Check if it intercepts the DUT
    // Spatial Cut Sensor Edge = 0, TODO:: Take it from config
    if(!m_detector->hasIntercept(track.get(), spatial_cut_sensoredge_)) {
        LOG(DEBUG) << " - track outside DUT area";
        return false;
    }

    // Check that track is within region of interest using winding number algorithm
    if(!m_detector->isWithinROI(track.get())) {
        return false;
    }

    // Check that it doesn't go through/near a masked pixel
    if(m_detector->hitMasked(track.get(), 1.)) {
        LOG(DEBUG) << " - track close to masked pixel";
        return false;
    }
    return true;
}

StatusCode WFAnalysis::run(const std::shared_ptr<Clipboard>& clipboard) {

    // Loop over all detectors
    for(auto& detector : get_detectors()) {
        // Get the detector name
        std::string detectorName = detector->getName();
        LOG(DEBUG) << "Detector with name " << detectorName;
    }

    // Get the telescope tracks from the clipboard
    auto tracks = clipboard->getData<Track>();

    // Get the waveform vectors
    auto pixels = clipboard->getData<Waveform>("DSO9254A_0");

    double risetime;
    double amplitude;
    double baseline;
    double peak;
    double ninety_amp;
    double ten_amp;

    int smooth_width = 9;
    double smooth;

    std::vector<double> risetime_channels;
    std::vector<double> waveform_vector(2500);
    std::vector<std::vector<double>> waveform_vector_collection;
    std::vector<double> time(2500);
    // double risetime_channels[4]={};
    int channel = 0;

    for(auto pixel : pixels) {

        // Hit map
        int x_pixel = pixel->column();
        int y_pixel = pixel->row();

        // Waveform analysis

        auto wave = pixel->waveform();

        TH1D* Waveform_to_analyze = new TH1D("Waveform", "Waveform", 2499, -299, -49);

        TH1D* invert_histo_baseline = new TH1D("Std Dev_base", " Std Dev_base", 2500, 0.12, 0.18);

        TH1D* smooth_waveform = new TH1D("Smooth wave", "Smooth wave", 2480, -299, -49);

        TCanvas* c1 = new TCanvas("c1", "c1", 800, 450);
        // TCanvas* c2 = new TCanvas("c2", "c2", 800, 450);

        if(wave.waveform.size() == 2500) {
            // Fill histogram with all the waveforms
            for(std::vector<int>::size_type i = 0; i < 2500; i++) {
                Raw_waveform->Fill(-300 + i * (250 / 2500.), wave.waveform[i]);

                // Fill vectors
                waveform_vector[i] = wave.waveform[i];
                time[i] = -300 + i * (250 / 2500.);

                // Fill the analyze histogram
                Waveform_to_analyze->SetBinContent(i, wave.waveform[i]);
                // std::cout<<Raw_waveform->GetBin(i)<<std::endl;

                // std::cout<<baseline<<std::endl;
            }
        }

        // Smooth the waveform to reduce noise
        for(int i = 0; i < Waveform_to_analyze->GetNbinsX(); i++) {
            for(int k = -smooth_width; k < smooth_width + 1; k++) {

                if(Waveform_to_analyze->GetBinContent(k + i) != 0) {
                    smooth += Waveform_to_analyze->GetBinContent(k + i) / (2 * smooth_width + 1);
                    if(k == smooth_width && smooth > 0) {
                        smooth_waveform->SetBinContent(Waveform_to_analyze->GetBin(i), smooth);
                    }
                }

                else {
                }
            }
            smooth = 0;
        }

        // Fill another histogram to get baseline for discrimination
        for(int i = 1; i < smooth_waveform->GetNbinsX(); i++) {

            if(-300 < smooth_waveform->GetBinCenter(i) && smooth_waveform->GetBinCenter(i) < -250) {
                invert_histo_baseline->Fill(smooth_waveform->GetBinContent(i));
            }

            else {
            }
        }

        double baseline_hist = invert_histo_baseline->GetMean();
        double peak_hist = smooth_waveform->GetMinimum() + invert_histo_baseline->GetStdDev();
        double amplitude_hist = baseline_hist - peak_hist;
        Amplitude_hist->Fill(amplitude_hist);

        if(waveFit_ && amplitude_hist > chargeCut_ / chargeScale_) // Implement a threshold from configuration
        {

            // Fill hit map
            Pixel_Hits->Fill(x_pixel, y_pixel, 1);

            ROOT::EnableImplicitMT(30);

            ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2", "MINIMIZE");

            //  Fitting a function
            // fitting error function on triggered pixels to get the range where the signal begins
            TF1* erf_full = new TF1("erf_full", "[0]*TMath::Erf((x-[1])/[2])+[3]", -299, -49);

            // Some automated guessing of the initial parameters
            erf_full->SetParameter(0, -amplitude_hist / 2.);
            erf_full->SetParameter(1, ninety_amp);
            erf_full->SetParameter(2, 10);
            erf_full->SetParameter(3, baseline_hist);

            TFitResultPtr fp = smooth_waveform->Fit(erf_full, "QN");
            TFitResultPtr fp2 = smooth_waveform->Fit(erf_full, "QNR+S");
            TFitResultPtr fp3 = smooth_waveform->Fit(erf_full, "Q+S"); // N required to run in screen

            // Check if the full fit works and if yes, refine it on the signal part (fp3==0 means converge and fp3==1 the
            // fit exist but did not find a root)
            if(fp3 == 0) {

                baseline = erf_full->GetMaximum();

                TF1* erf_rise = new TF1(
                    "erf_rise", "[0]*TMath::Erf((x-[1])/[2])+[3]", erf_full->GetX(baseline - 0.08 * amplitude_hist), -49);

                erf_rise->SetParameter(0, erf_full->GetParameter(0));
                erf_rise->SetParameter(1, erf_full->GetParameter(1));
                erf_rise->SetParameter(2, erf_full->GetParameter(2));
                erf_rise->SetParameter(3, erf_full->GetParameter(3));

                TFitResultPtr fp6 = smooth_waveform->Fit(erf_rise, "QR+S"); // N required to run in screen

                peak = erf_rise->GetMinimum();
                amplitude = baseline - peak;
                risetime = abs(erf_rise->GetX(baseline - 0.9 * amplitude) - erf_rise->GetX(baseline - 0.1 * amplitude));
                count++;
            }

            // As few events may fail due to having a very short baseline,
            // let's calculate from the histogram where the signal starts to fit it

            else {

                // check for 10 and 90 percent of the peak, cannot use FindLastBinAbove due to the waveforms being
                // negative
                bool check_10amp = 0;
                bool check_90amp = 0;
                for(int i = 1; i < Waveform_to_analyze->GetNbinsX(); i++) // avoid the histogram edge i=0
                {
                    if((baseline_hist - 0.9 * amplitude_hist) > Waveform_to_analyze->GetBinContent(i) && check_90amp == 0) {

                        ninety_amp = Waveform_to_analyze->GetXaxis()->GetBinCenter(i);
                        check_90amp = 1;
                    }

                    if((baseline_hist - 0.05 * amplitude_hist) > Waveform_to_analyze->GetBinContent(i) && check_10amp == 0) {
                        // cout << histo->GetXaxis()->GetBinCenter(i) << endl;
                        ten_amp = Waveform_to_analyze->GetXaxis()->GetBinCenter(i);
                        // cout << ten_amp << endl;
                        check_10amp = 1;
                    }
                }

                TF1* erf_rise = new TF1("erf_rise", "[0]*TMath::Erf((x-[1])/[2])+[3]", ten_amp, -50);

                erf_rise->SetParameter(0, -amplitude_hist / 2.);
                erf_rise->SetParameter(1, ninety_amp);
                erf_rise->SetParameter(2, 10);
                erf_rise->SetParameter(3, baseline_hist);

                TFitResultPtr fp6 = smooth_waveform->Fit(erf_rise, "QR+S");

                baseline = erf_rise->GetMaximum();
                peak = erf_rise->GetMinimum();
                amplitude = baseline - peak;
                risetime = abs(erf_rise->GetX(baseline - 0.9 * amplitude) - erf_rise->GetX(baseline - 0.1 * amplitude));
            }

            // Here to draw the waveforms and their fits
            // smooth_waveform->Draw();
            // c1->Write();

            // Fill risetime histograms and vector
            Rise_time->Fill(risetime);
            // risetime_channels[channel]=risetime;
            risetime_channels.push_back(risetime);
            waveform_vector_collection.push_back(waveform_vector);

            // channel++;
            if(risetime > 0 || risetime == 0) {
                Amplitude_Risetime->Fill(amplitude_hist, risetime);
            }

            // Waveform_to_analyze->Reset("ICESM");
        } // Waveform Analysis with fit

        if(!waveFit_ && amplitude_hist > chargeCut_ / chargeScale_) {

            for(int i = 0; i < Waveform_to_analyze->GetNbinsX(); i++) {
                for(int k = -smooth_width; k < smooth_width + 1; k++) {

                    if(Waveform_to_analyze->GetBinContent(k + i) != 0) {
                        smooth += Waveform_to_analyze->GetBinContent(k + i) / (2 * smooth_width + 1);
                        if(k == smooth_width && smooth > 0) {
                            smooth_waveform->SetBinContent(Waveform_to_analyze->GetBin(i), smooth);
                        }
                    }

                    else {
                    }
                }
                smooth = 0;
            }

            Waveform_to_analyze->Draw();
            // c1->Write();

            smooth_waveform->Draw();
            // c1->Write();

            baseline = smooth_waveform->GetMaximum();
            peak = smooth_waveform->GetMinimum();
            amplitude = baseline - peak;
            risetime = abs(
                smooth_waveform->GetXaxis()->GetBinCenter(smooth_waveform->FindLastBinAbove(baseline - 0.9 * amplitude)) -
                smooth_waveform->GetXaxis()->GetBinCenter(smooth_waveform->FindLastBinAbove(baseline - 0.1 * amplitude)));

            // std::cout << amplitude << std::endl;
            Rise_time->Fill(risetime);

        } // Waveform analysis without fit

        delete invert_histo_baseline;
        delete Waveform_to_analyze;
        delete smooth_waveform;
        delete c1;
        // delete c2;

    } // Waveforms

    // Tracking position starts here
    for(auto& track : tracks) {

        auto globalIntercept = m_detector->getIntercept(track.get());
        auto localIntercept = m_detector->globalToLocal(globalIntercept);

        // DUT geometry
        if(!acceptTrackDUT(track))
            continue;

        // Get the event:
        auto event = clipboard->getEvent();

        // Discard tracks which are very close to the frame edges
        if(fabs(track->timestamp() - event->end()) < time_cut_frameedge_) {
            continue;
        } else if(fabs(track->timestamp() - event->start()) < time_cut_frameedge_) {
            continue;
        }

        // Calculate in-pixel position of track in microns
        auto inpixel = m_detector->inPixel(localIntercept);
        auto xmod_um = inpixel.X() * 1000.; // convert mm -> um
        auto ymod_um = inpixel.Y() * 1000.; // convert mm -> um

        for(auto assoc_cluster : track->getAssociatedClusters(m_detector->getName())) {

            // if closest cluster should be used continue if current associated cluster is not the closest one
            if(use_closest_cluster_ && track->getClosestCluster(m_detector->getName()) != assoc_cluster) {
                continue;
            }

            // double *seed_risetime=std::min_element(risetime_channels, risetime_channels+channel);
            // std::cout << "Rise time of the seed: "<< seed_risetime << std::endl;
            if(risetime_channels.size() > 0) {
                double seed_risetime = *min_element(risetime_channels.begin(), risetime_channels.end());

                if(seed_risetime > 0 || seed_risetime == 0) {
                    In_pixel_risetime->Fill(xmod_um, ymod_um, seed_risetime);

                    if(waveform_vector_collection.size() > 0) {
                        if(xmod_um <= 1.5 && xmod_um >= -1.5 && ymod_um <= 1.5 && ymod_um >= -1.5) {

                            TH1D* Waveform_center = new TH1D("Waveform_center", "Waveform", 2499, -299, -49);
                            for(std::vector<int>::size_type i = 0; i < 2500; i++) {

                                Waveform_center->SetBinContent(i, waveform_vector_collection.at(0).at(i));
                            }

                            Rise_time_center->Fill(seed_risetime);
                            local_directory_center->cd();
                            Waveform_center->Write();
                            delete Waveform_center;
                            directory->cd();
                        }

                         if(xmod_um <=-9.5  && xmod_um >= -12.5 && ymod_um <= -9.5 && ymod_um >= -12.5) {

                            TH1D* Waveform_corner = new TH1D("Waveform_corner", "Waveform", 2499, -299, -49);
                            for(std::vector<int>::size_type i = 0; i < 2500; i++) {

                                Waveform_corner->SetBinContent(i, waveform_vector_collection.at(0).at(i));
                            }

                             Rise_time_corner->Fill(seed_risetime);
                            local_directory_corner->cd();
                            Waveform_corner->Write();
                            delete Waveform_corner;
                            directory->cd();
                        }
                    }
                } // risetime_channels>0
            }

            // Timestamps output file
            // std::ofstream myfile;
            // myfile.open("timestamp_corry.txt", std::ios_base::app);
            // myfile << m_eventNumber << "\t" << assoc_cluster->timestamp() << "\t" << track->timestamp() << "\n";
            // myfile.close();
        }
    }

    // Increment event counter
    m_eventNumber++;

    // risetime_channels.clear();
    // std::vector<double>().swap(risetime_channels);
    // delete risetime_channels;

    // Return value telling analysis to keep running
    return StatusCode::Success;
}

void WFAnalysis::finalize(const std::shared_ptr<ReadonlyClipboard>&) {

    LOG(DEBUG) << "Analysed " << m_eventNumber << " events";
    std::cout << "Number of waveforms: " << count << std::endl;
}
