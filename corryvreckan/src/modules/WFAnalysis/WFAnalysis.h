/**
 * @file
 * @brief Definition of module WFAnalysis
 *
 * @copyright Copyright (c) 2020 CERN and the Corryvreckan authors.
 * This software is distributed under the terms of the MIT License, copied verbatim in the file "LICENSE.md".
 * In applying this license, CERN does not waive the privileges and immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 * SPDX-License-Identifier: MIT
 */

#include <TCanvas.h>
#include <TH1F.h>
#include <TH2F.h>
#include <iostream>
#include <TProfile2D.h>
#include <TSpline.h>
#include <vector>

#include "core/module/Module.hpp"
#include "objects/Cluster.hpp"
#include "objects/Pixel.hpp"
#include "objects/Track.hpp"

namespace corryvreckan {
    /** @ingroup Modules
     * @brief Module to do function
     *
     * More detailed explanation of module
     */
    class WFAnalysis : public Module {

    public:
        /**
         * @brief Constructor for this unique module
         * @param config Configuration object for this module as retrieved from the steering file
         * @param detector Pointer to the detector for this module instance
         */
        WFAnalysis(Configuration& config, std::shared_ptr<Detector> detector);

        /**
         * @brief [Initialise this module]
         */
        void initialize() override;

        /**
         * @brief [Run the function of this module]
         */
        StatusCode run(const std::shared_ptr<Clipboard>& clipboard) override;

        /**
         * @brief [Finalise module]
         */
        void finalize(const std::shared_ptr<ReadonlyClipboard>& clipboard) override;

    private:

        virtual bool acceptTrackDUT(const std::shared_ptr<Track>& track);
        

         // Member variables
        ROOT::Math::XYPoint inpixelBinSize_;
        double time_cut_frameedge_;
        double spatial_cut_sensoredge_;
        double chi2_ndof_cut_;
        double chargeCut_;
        double chargeScale_;
        bool use_closest_cluster_;
        bool waveFit_;
        bool Do_Once=true;
        //std::vector<double> risetime_channels;


        //std::shared_ptr<Detector> detector;
        std::shared_ptr<Detector> m_detector;
        int m_eventNumber;
        int count;

        //Histograms
        TH2F* Raw_waveform;
        //TH1F* Waveform_to_analyze;
        TH1D* Rise_time;
        TH1D* Rise_time_center;
        TH1D* Rise_time_corner;
        TH1D* Amplitude_hist;
        TProfile2D* In_pixel_risetime;
        TH2D* Pixel_Hits;
        TH2D* Amplitude_Risetime;
        //TSpline
        TSpline3* wave;
        TDirectory* local_directory_center;
        TDirectory* local_directory_corner;
        TDirectory* directory;
    };

} // namespace corryvreckan
